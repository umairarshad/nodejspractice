 const fs = require('fs');
// //create a file
// fs.writeFile('example.txt', "this is an example", (err)=>{//file is creating
//     if (err)
//         console.log(err);
//     else
//         console.log('File successfully created');

//         fs.readFile('example.txt','utf8', (err, file)=>{
//             if (err)
//                 console.log(err);
//              else
//                console.log(file);
//         })
// })


//rename the file
// fs.rename('example.txt', 'example2.txt', (err)=>{
//     if (err)
//         console.log(err);
//     else 
//         console.log('Successfully renamed the file');
// });


//append data in file
// fs.appendFile('example.txt', 'Some data being appended', (err)=>{
//     if (err)
//         console.log(err);
//     else 
//         console.log('Successfully appended data to file');
// });

//delet file
fs.unlink('example.txt', (err)=>{
    if (err)
        console.log(err);
    else 
        console.log('Successfully deleted the file');
});